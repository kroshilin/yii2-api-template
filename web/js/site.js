/**
 * quiz questions control
 */
$(function(){

    $('.add-option').click(function(){
        var cpy = $('.copy-option').last().clone();
        var inputText = cpy.find('input[name*=point]');
        inputText.val('');
        inputText.attr('name', inputText.attr('name').replace(inputText.data('index'),inputText.data('index')+1));
        inputText.attr('data-index', inputText.data('index')+1);
        var inputIndex = cpy.find('input[name*=id]');
        inputIndex.val(inputIndex.data('index')+1);
        inputIndex.attr('name', inputIndex.attr('name').replace(inputIndex.data('index'),inputIndex.data('index')+1));
        inputIndex.attr('data-index', inputIndex.data('index')+1);
        var tarea = cpy.find('textarea');
        tarea.val('');
        tarea.attr('name', tarea.attr('name').replace(tarea.data('index'),tarea.data('index')+1));
        tarea.attr('data-index', tarea.data('index')+1);
        $('.copy-option').last().after(cpy);
        return false;
    });

    $('body').on('click', '.quiz-question-form .delete', function(){
        $(this).parent().slideUp().remove();
        return false;
    });

});