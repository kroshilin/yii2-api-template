<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "serial".
 *
 * @property integer $id
 * @property string $title
 * @property string $text
 * @property string $thumbnail_path
 * @property string $thumbnail_base_url
 * @property integer $created_at
 * @property integer $updated_at
 */
class Serial extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'serial';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text'], 'required'],
            [['text'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            ['file', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'text' => 'Описание',
            'thumbnail_path' => 'Обложка',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'file' => 'Обложка',
        ];
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'file',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            TimestampBehavior::className(),
        ];
    }
}