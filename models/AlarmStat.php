<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "alarm_stat".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $motions
 * @property string $factors
 * @property integer $efficiency
 * @property integer $time
 * @property integer $started_at
 * @property integer $ended_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class AlarmStat extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'alarm_stat';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'time', 'created_at', 'updated_at'], 'integer'],
            ['efficiency', 'double'],
            [['ended_at', 'started_at', 'time'], 'required'],
            [['ended_at', 'started_at'], 'filter', 'filter' => 'strtotime'],
            [['motions', 'factors'], 'string'],
            ['user_id', 'default', 'value' => Yii::$app->user->id],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'Юзер ID',
            'time' => 'Время сна, мин',
            'efficiency' => 'Эффективность сна',
            'motions' => 'Количиство часов',
            'factors' => 'Факторы',
            'started_at' => 'Начало сна',
            'ended_at' => 'Окончание сна',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->user->refreshSleepStatistic($this);
    }

}
