<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "music_album".
 *
 * @property integer $id
 * @property string $title
 *
 * @property MusicTrack[] $musicTracks
 */
class MusicAlbum extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'music_album';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название альбома',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлён',
        ];
    }
    
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMusicTracks()
    {
        return $this->hasMany(MusicTrack::className(), ['album_id' => 'id']);
    }
    
    public static function getMusicAlbums()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
