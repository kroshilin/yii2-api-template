<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use dosamigos\taggable\Taggable;

/**
 * This is the model class for table "video".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $video_id
 * @property string $thumbnail_path
 * @property string $thumbnail_base_url
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $episode
 */
class Video extends \yii\db\ActiveRecord
{
    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'video';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'text', 'episode', 'video_id'], 'required'],
            [['description', 'text'], 'string'],
            [['episode'], 'integer'],
            [['title', 'video_id', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            ['file', 'safe'],
            [['tagNames'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'description' => 'Описание',
            'text' => 'Содержание',
            'video_id' => 'ID видео на rutube.ru',
            'thumbnail_path' => 'Обложка',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Добавлено',
            'updated_at' => 'Обновлено',
            'file' => 'Обложка',
            'episode' => 'Номер серии',
        ];
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'file',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            TimestampBehavior::className(),
            [
                'class' => Taggable::className(),
                'name' => 'title'
            ]
        ];
    }

    public function getVideoUrl()
    {
        return  Yii::$app->params['rutubeVideoUrl'].$this->video_id.'/';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Dream::className(), ['id' => 'dream_id'])->viaTable('dream_video', ['video_id' => 'id']);
    }
}
