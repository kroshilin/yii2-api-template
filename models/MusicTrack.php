<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "music_track".
 *
 * @property integer $id
 * @property integer $number
 * @property string $title
 * @property integer $duration
 * @property string $description
 * @property string $music_track_path
 * @property string $music_track_base_url
 * @property integer $created_at
 * @property integer $updated_at
 */
class MusicTrack extends \yii\db\ActiveRecord
{
    public $trackFile;
    public $coverFile;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'music_track';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'number', 'duration', 'album_id'], 'required'],
            [['number', 'duration', 'album_id'], 'integer'],
            [['description'], 'string'],
            [['title', 'music_track_path', 'music_track_base_url', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            [['trackFile', 'coverFile'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'album_id' => 'Альбом',
            'number' => 'Номер трека',
            'title' => 'Название',
            'duration' => 'Продолжительность (сек)',
            'description' => 'Описание',
            'music_track_path' => 'Путь к файлу',
            'music_track_base_url' => 'Базовый URL',
            'thumbnail_path' => 'Изображение',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлён',
            'trackFile' => 'Аудио файл',
            'coverFile' => 'Обложка',
        ];
    }
    
    public function behaviors()
    {
        return [
            'coverFile' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'coverFile',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            'trackFile' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'trackFile',
                'pathAttribute' => 'music_track_path',
                'baseUrlAttribute' => 'music_track_base_url',
            ],
            TimestampBehavior::className(),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMusicAlbum()
    {
        return $this->hasOne(MusicAlbum::className(), ['id' => 'album_id']);
    }
    
}
