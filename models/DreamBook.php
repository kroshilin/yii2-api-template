<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "dream_book".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $thumbnail_path
 * @property string $thumbnail_base_url
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Dream[] $dreams
 */
class DreamBook extends \yii\db\ActiveRecord
{

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dream_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['description'], 'string'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            ['file', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'thumbnail_path' => 'Изображение',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'file' => 'Обложка',
        ];
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'file',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDreams()
    {
        return $this->hasMany(Dream::className(), ['dream_book_id' => 'id']);
    }

    public static function getDreamBooks()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'title');
    }
}
