<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "diary".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $place
 * @property string $condition
 * @property integer $user_id
 * @property integer $dreamed_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property User $user
 */
class Diary extends \yii\db\ActiveRecord
{
    public $tagIds = [];
    public $tagsToUnlink;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'diary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['description'], 'string', 'max' => 1800],
            ['user_id', 'filter', 'filter' =>
                function ($value) {
                    return Yii::$app->user->id;
                }
            ],
            [['dreamed_at'], 'filter', 'filter' => 'strtotime'],
            [['title', 'tagsToUnlink'], 'string', 'max' => 255],
            [['condition', 'place'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название',
            'description' => 'Описание',
            'condition' => 'Состояние',
            'place' => 'Место',
            'user_id' => 'Пользователь',
            'dreamed_at' => 'Снилось',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDreams()
    {
        return $this->hasMany(Dream::className(), ['id' => 'dream_id'])
            ->viaTable('dream_diary', ['diary_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->findTags();
    }

    private function findTags()
    {
        $words = $this->prepareTextForSearch();
        Yii::$app->db->createCommand("DELETE from dream_diary where diary_id = $this->id")
            ->execute();
        foreach ($words as $word) {
            $tag = Yii::$app->db
                ->createCommand("SELECT id, title, levenshtein_ratio('$word', title) as l from dream where levenshtein_ratio('$word', title)>".Yii::$app->params['levenshteinRatio']." ORDER BY l DESC")
                ->queryOne();
            if ($tag['id']) {
                $dreamsFromOtherBooks = Dream::findAll(['title' => $tag['title']]);
                foreach ($dreamsFromOtherBooks as $dream) {
                    Yii::$app->db->createCommand("INSERT INTO dream_diary (dream_id, diary_id, origin) VALUES ($dream->id, $this->id, '$word')")
                        ->execute();
                }
            }
        }
    }

    /**
     * @return array
     */
    private function prepareTextForSearch()
    {
        $words = [];
        $arr = preg_split("/[\s,.]+/", $this->description);
        foreach ($arr as $word) {
            if (strlen($word) >= Yii::$app->params['wordMinLength']) {
                $words[] = $word;
            }
        }
        return $words;
    }
}
