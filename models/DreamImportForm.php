<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

use app\models\Dream;
use yii\web\UploadedFile;

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

class DreamImportForm extends \yii\base\Model
{
    public $check = null;
    public $title = null;
    public $description = null;
    public $text = null;
    public $isExistInProject = null;   
    public $episodeId = null;          
    public $episodeTitle = null;       
    public $mood_id = null;            
    public $thumbnail_path = null;     
    public $thumbnail_base_url = null; 
    
    public $dream_book_id = null; 
    
    const UPLOAD_PATH = '/uploads/dreamImportData/';
    
    public function rules()
    {
        return [
            [['check', 'mood_id', 'isExistInProject', 'episodeId', 'dream_book_id'], 'integer'],
            [['description', 'text', 'episodeTitle'], 'string'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
        ];
    }
    
    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'description' => 'Описание',
            'text' => 'Текст',
            'thumbnail_path' => 'Изображение',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'file' => 'Обложка',
            'mood_id' => 'Настроение',
            'isExistInProject' => 'Присутствует в проекте',
            'episodeId' => 'Номер серии',
            'episodeTitle' => 'Название серии',
        ];
    }
    
    public static function getModelsFromImportZipFile($import_src)
    {
        if($importFolderName = DreamImportForm::unzipToImportTempFolder($import_src))
        {
            return DreamImportForm::getModelsFromImportTempFolder($importFolderName);
        } else {
            return [];
        }
    }
    
    public static function unzipToImportTempFolder($import_src)
    {
        $zipName = date("Ymd_His");
        if($import_src->saveAs(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$zipName.'.'.$import_src->extension)) {
            //Zip-файл перемещён в папку uploads/dreamImportData
            $zipAr = new \ZipArchive();
            if ($zipAr->open(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$zipName.'.'.$import_src->extension) === true) {
                $zipAr->extractTo(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$zipName);
                $zipAr->close();
                unlink(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$zipName.'.'.$import_src->extension); //Drop zip file
            } else {
                Yii::$app->session->setFlash('danger', 'Zip-архив повреждён');
                return false;
            }
            
            //Приводим расширения всех файлов директории к нижнему регистру
            DreamImportForm::setToLowerEachFileExtension(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$zipName);
        }
        else
        {
          Yii::$app->session->setFlash('danger', 'Ошибка при загрузке файла на сервер');
          return false;
        }

        return $zipName;
    }
    
    public static function getModelsFromImportTempFolder($folderName)
    {
        $structImportData = [];
        $impFolder = opendir(Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$folderName);
        while (false !== ($filename = readdir($impFolder))) {
            $fullpath = Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$folderName.'/';
            if(pathinfo($fullpath.$filename, PATHINFO_EXTENSION) == 'xlsx') {
                $reader = ReaderFactory::create(Type::XLSX);
                $reader->open($fullpath.$filename);
                
                $moodTitles = array_keys(Mood::getMoods());
                
                foreach ($reader->getSheetIterator() as $sheet) {
                    foreach ($sheet->getRowIterator() as $row) {
                        if(empty((int)$row[0])) continue; //Пропускаем строки заголовков
                        if(empty($row[2])) break; //Останавливаемся, когда встречаем строку с пустым "описанием сна"
                        
                        $moodIdx = array_search('да', array_map('mb_strtolower', [$row[7], $row[8], $row[9], $row[10]]));
                        $imagePath = ( glob($fullpath.trim($row[0]).".{jpg,jpeg,png,gif,bmp}", GLOB_BRACE) ? : null );

                        $structImportData[] = new DreamImportForm([
                            'title'              => (trim($row[1]) ? : null),
                            'description'        => (trim($row[2]) ? : null),
                            'text'               => (trim($row[3]) ? : null),
                            'isExistInProject'   => (mb_strtolower(trim($row[4])) == 'да'),
                            'episodeId'          => ((int)$row[5] ?: null),
                            'episodeTitle'       => (trim($row[6]) ? : null),
                            'mood_id'            => ($moodIdx !== false ? $moodTitles[$moodIdx] : null),
                            'thumbnail_path'     => (is_array($imagePath) ? str_replace($_SERVER['DOCUMENT_ROOT'], '', $imagePath[0]) : null),
                            'thumbnail_base_url' => (is_array($imagePath) ? $imagePath[0] : null),
                        ]);
                    }
                }
                
            }
        }
        
        return [
            "models" => $structImportData, 
            "folder" => Yii::getAlias('@webroot').DreamImportForm::UPLOAD_PATH.$folderName
        ];
    }
    
    public function save()
    {
        if($this->check) {
                    
            $dream = new Dream();
            $dream->load($this->toArray(), '');
            
            if(!empty($this->thumbnail_path))
            {
              /** File prepare **/
              $uploadedFile = new UploadedFile();
              
              $fileInfo = pathinfo($this->thumbnail_path);
              
              $uploadedFile->name = $fileInfo['basename'];
              $uploadedFile->tempName = $fileInfo['dirname'].'/'.$fileInfo['basename'];
              $uploadedFile->type = 'image/'.$fileInfo['extension'];
              $uploadedFile->size = filesize($this->thumbnail_path);
              
              $dream->thumbnail_path = Yii::$app->fileStorage->save($uploadedFile);
              $dream->thumbnail_base_url = Yii::$app->fileStorage->baseUrl;
            }
            
            $dream->detachBehavior('file');
            
            /* Связка с серией */
            if ($dream->save() && !empty($this->episodeId)) {
                if($video = Video::findOne(['episode' => $this->episodeId])) {
                    $dream->link('videos', $video);
                }
            }
        }
    }
    
    public static function setToLowerEachFileExtension($folder) {
        $handle = opendir($folder);
        while (false !== $f = readdir($handle)) {
            if ($f != '.' && $f != '..') {
                $filePath = "$folder/$f";
                $fileInfo = pathinfo($filePath);
                if (is_file($filePath)) {
                    rename(
                      $fileInfo['dirname'].'/'.$fileInfo['basename'],
                      $fileInfo['dirname'].'/'.$fileInfo['filename'].'.'.mb_strtolower($fileInfo['extension']));
                } elseif (is_dir($filePath)) {
                    self::setToLowerEachFileExtension($filePath);
                }
            }
        }
        closedir($handle);
    }
}
