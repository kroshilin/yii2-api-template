<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "quiz_user_quiz".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $quiz_id
 * @property integer $started_at
 * @property integer $finished_at
 *
 * @property QuestionResults[] $questionResults
 * @property Quiz $quiz
 * @property User $user
 */
class QuizUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_user_quiz';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'quiz_id', 'started_at', 'finished_at'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'quiz_id' => Yii::t('app', 'Quiz ID'),
            'started_at' => Yii::t('app', 'Started At'),
            'finished_at' => Yii::t('app', 'Finished At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     * @param integer $questionId
     */
    public function getQuestionResults($questionId = null)
    {
        $query = $this->hasMany(QuestionResult::className(), ['quiz_user_quiz_id' => 'id']);
        if ($questionId) {
            $query->where(['question_id' => $questionId]);
        }
        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuiz()
    {
        return $this->hasOne(Quiz::className(), ['id' => 'quiz_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return int
     */
    public function getMistakes()
    {
        return QuestionResult::find()->where(['quiz_user_quiz_id' => $this->id, 'result' => '1'])->count();
    }
}
