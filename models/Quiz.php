<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "quiz".
 *
 * @property integer $id
 * @property integer $status
 * @property string $title
 * @property string $description
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Question[] $questions
 * @property QuizResult[] $results
 */
class Quiz extends \yii\db\ActiveRecord
{

    const STATUS_UNPUBLISHED = 0;
    const STATUS_PUBLISHED = 1;

    public $file;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz';
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'file',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['status'], 'integer'],
            [['description'], 'string'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            ['file', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Опубликован?',
            'file' => 'Изображение',
            'title' =>  'Название',
            'description' => 'Описание',
            'created_at' =>  'Добавлен',
            'updated_at' =>  'Обновлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['quiz_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(QuizResult::className(), ['quiz_id' => 'id']);
    }

    /**
     * Save user answers
     * @param $result
     * @return boolean
     */
    public function saveResult($result)
    {
        $answers = 0;
        $quizUser = QuizUser::findOne([
            'finished_at' => null,
            'user_id' => Yii::$app->user->id,
            'quiz_id' => $this->id
        ]);
        if ($quizUser) {
            foreach ($result['answer'] as $key => $answer) {
                $quizResult = QuestionResult::findOne([
                    'quiz_user_id' => $quizUser->id,
                    'user_id' => Yii::$app->user->id,
                    'question_id' => $key,
                ]);
                if (!$quizResult) {
                    $quizResult = new QuestionResult([
                        'quiz_user_id' => $quizUser->id,
                        'user_id' => Yii::$app->user->id,
                        'question_id' => $key,
                    ]);
                }
                $quizResult->result = is_array($answer) ? implode(',', $answer) : $answer;

                if ($quizResult->save()){
                    $answers++;
                }
            }
            if (count($this->questions) != $answers){
                Yii::$app->session->setFlash('error', Yii::t('app','You answered not all questions'));
                return false;
            }
            $quizUser->finished_at = time();
            $quizUser->save();
            Yii::$app->session->setFlash('success', Yii::t('app','Your results saved!'));
            return true;
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app','Could not save results of quiz'));
            return false;
        }
    }

    public function UserQuestionAnswer($question_id){
        $quizUser = QuizUser::findOne([
            'finished_at' => null,
            'user_id' => Yii::$app->user->id,
            'quiz_id' => $this->id
        ]);
        if ($quizUser) {
            $quizResult = QuestionResult::findOne([
                'user_id' => Yii::$app->user->id,
                'quiz_user_id' => $quizUser->id,
                'quiz_question_id' => $question_id
            ]);
            return ($quizResult)?$quizResult->result:false;
        } else {
            return false;
        }
    }

    public function isFinished()
    {
       return ($this->active_until && (time()>$this->active_until));
    }

    public static function getStatuses()
    {
        return [
            self::STATUS_PUBLISHED  => 'Опубликован',
            self::STATUS_UNPUBLISHED => 'Не опубликован',
        ];
    }

}
