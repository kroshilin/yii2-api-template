<?php

namespace app\models;

use app\modules\api\common\models\resources\QuizResult;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "quiz_question_result".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $question_id
 * @property integer $points
 * @property integer $answer
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Quiz $quiz
 * @property User $quizUserQuiz
 * @property Question $question
 */
class QuestionResult extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'quiz_question_result';
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className()
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['user_id', 'filter', 'filter' =>
                                function ($value) {
                            return Yii::$app->user->id;
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'Quiz User Quiz ID'),
            'question_id' => Yii::t('app', 'Quiz Question ID'),
            'points' => Yii::t('app', 'Points'),
            'answer' => Yii::t('app', 'Answer'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(Question::className(), ['id' => 'question_id']);
    }

    public static function saveResults($results)
    {
        foreach ($results as $result) {
            $question = Question::findOne($result['question_id']);
            $options = $question->answers;
            if ($model = QuestionResult::findOne(['question_id' => $result['question_id'], 'user_id' => Yii::$app->user->id])) {
                $model->answer = $result['answer_id'];
                $model->points = $options[$result['answer_id']]['point'];
            } else {
                $model = new QuestionResult([
                    'user_id' => Yii::$app->user->id,
                    'question_id' => $result['question_id'],
                    'points' => $options[$result['answer_id']]['point'],
                    'answer' => $result['answer_id'],
                ]);
            }
            $model->save();
        }
        return;
    }

    public static function calculatePoints($quiz_id)
    {
        $points = 0;
        $questionIds = ArrayHelper::map(Question::findAll(['quiz_id' => $quiz_id]), 'id', 'id');
        $questionResults = self::findAll(['user_id' => Yii::$app->user->id, 'question_id' => $questionIds]);
        foreach ($questionResults as $qr) {
            $points += $qr->points;
        }
        return [
            'result' => $questionResults ? QuizResult::find()->where("$points BETWEEN min_points and max_points and quiz_id = $quiz_id")->one() : '',
            'answers' => $questionResults
        ];
    }
}
