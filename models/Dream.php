<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "dream".
 *
 * @property integer $id
 * @property integer $dream_book_id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property string $thumbnail_path
 * @property string $thumbnail_base_url
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $frequency
 *
 * @property DreamBook $dreamBook
 * @property array $videos
 */
class Dream extends \yii\db\ActiveRecord
{
    public $file;
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dream';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'description', 'text'], 'required'],
            [['dream_book_id', 'mood_id'], 'integer'],
            [['description', 'text'], 'string'],
            [['title', 'thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 255],
            ['file', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'dream_book_id' => 'Сонник',
            'title' => 'Название',
            'description' => 'Описание',
            'text' => 'Текст',
            'thumbnail_path' => 'Изображение',
            'thumbnail_base_url' => 'Базовый URL изображения',
            'created_at' => 'Создан',
            'updated_at' => 'Обновлен',
            'file' => 'Обложка',
            'mood_id' => 'Настроение',
            'frequency' => 'Частота использования'
        ];
    }

    public function behaviors()
    {
        return [
            'file' => [
                'class' => 'trntv\filekit\behaviors\UploadBehavior',
                'attribute' => 'file',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url',
            ],
            TimestampBehavior::className(),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDreamBook()
    {
        return $this->hasOne(DreamBook::className(), ['id' => 'dream_book_id']);
    }

    public function getMood()
    {
        return $this->hasOne(Mood::className(), ['id' => 'mood_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVideos()
    {
        return $this->hasMany(Video::className(), ['id' => 'video_id'])
            ->viaTable('dream_video', ['dream_id' => 'id']);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        $this->runIndexer();
    }

    /**
     * запускает переиндексацию Sphinx
     */
    private function runIndexer()
    {
        Yii::$app->consoleRunner->run('sphinx/indexer');
    }
    
}
