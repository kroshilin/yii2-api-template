-- Накатить этот дамп после миграций
-- Добавляем юзера по умолчанию - admin/123456
INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`) VALUES
  (1, 'admin', '2qP07mVd4nJZCgbK3EN8fKMDUUfzHW4n', '$2y$13$OmNQHp7GZaoyePZznNDdIel4Fbp.uXMLpa8Uo.nl8IXZBkLXZqYnW', NULL, 'admin@local.dev', 10, 0, 0);

-- Дамп данных таблицы `auth_rule`
INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
  ('isAuthor', 'O:19:"app\\rbac\\AuthorRule":3:{s:4:"name";s:8:"isAuthor";s:9:"createdAt";i:1454056387;s:9:"updatedAt";i:1454056387;}', 1454056387, 1454056387),
  ('isProfileOwner', 'O:25:"app\\rbac\\ProfileOwnerRule":3:{s:4:"name";s:14:"isProfileOwner";s:9:"createdAt";i:1454056401;s:9:"updatedAt";i:1454056401;}', 1454056401, 1454056401),
  ('isUser', 'O:21:"app\\rbac\\UserRoleRule":3:{s:4:"name";s:6:"isUser";s:9:"createdAt";i:1454070747;s:9:"updatedAt";i:1454070747;}', 1454070747, 1454070747);

-- Дамп данных таблицы `auth_item`
INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
  ('admin', 1, 'Суперюзер', NULL, NULL, 1453117955, 1453117955),
  ('updateDiary', 2, 'Редактирование дневника', NULL, NULL, 1454066998, 1454066998),
  ('updateOwnDiary', 2, 'Редактирование совего дневника', 'isAuthor', NULL, 1454066972, 1454067072),
  ('user', 1, 'Зарегистрированные юзеры', 'isUser', NULL, 1454066940, 1454070774);

-- Дамп данных таблицы `auth_item_child`
INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('admin', 'updateDiary'),
('updateOwnDiary', 'updateDiary'),
('user', 'updateOwnDiary'),
('admin', 'user');

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1453117984);
-- Добавляем запись с информацией о сериале в таблицу serial
INSERT INTO `serial` (`id`, `title`, `text`, `thumbnail_path`, `thumbnail_base_url`, `created_at`, `updated_at`) VALUES
(1, 'СНЫ на ТВ3', 'Героями проекта «Сны», который стартует на самом таинственном российском телеканале с 11 января 2016 года, стала пара ученых – психотерапевт Галина Акимова (Яна Ляхович) и нейрофизиолог Кирилл Зерлинский (Андрей Журба). С помощью современных технологий эксперты погружают пациента в особое состояние, ведут по лабиринтам сна и интерпретируют зашифрованные сигналы подсознания. Наблюдая за работой экспертов Лаборатории осознанных сновидений, каждый зритель ТВ-3 сможет разобраться в себе.', '', '', 1453811424, 1453811424);

-- Добавляем записи в таблицу настроений
INSERT INTO `mood` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'позитивное', 1454326387, 1454326387),
(2, 'умеренное', 1454326446, 1454326446),
(3, 'тревожное', 1454326487, 1454326487),
(4, 'негативное', 1454326498, 1454326498);

-- Добавляем записи в таблицу музыкальных альбомов
INSERT INTO `music_album` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Засыпание', 1454326387, 1454326387),
(2, 'Пробуждение', 1454326446, 1454326446),
(3, 'Релакс', 1454326487, 1454326487),
(4, 'Медитция', 1454326498, 1454326498),
(5, 'Настроения', 1454326498, 1454326498),
(6, 'Другое', 1454326498, 1454326498);