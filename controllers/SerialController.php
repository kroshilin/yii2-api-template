<?php

namespace app\controllers;

use Yii;
use app\models\Serial;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * SerialController implements the CRUD actions for Serial model.
 */
class SerialController extends Controller
{
    public function actionIndex()
    {
        /* Выводим запись с описанием сериала для редактирования */
        $model = Serial::findOne(getenv('SERIAL_ROW_RETURN_ID'));

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Информация сохранена!');
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
