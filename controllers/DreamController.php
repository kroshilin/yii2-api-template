<?php

namespace app\controllers;

use Yii;
use app\models\Dream;
use app\models\DreamImportForm;
use app\models\search\DreamSearch;
use yii\web\Response;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use yii\filters\VerbFilter;



use yii\data\ArrayDataProvider;
use yii\helpers\FileHelper; 

/**
 * DreamController implements the CRUD actions for Dream model.
 */
class DreamController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Dream models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DreamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->with('dreamBook');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Dream model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dream();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Сон добавлен!');
            return $this->redirect(['update', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Экшн для получения списка тегов
     * @param $query
     * @return array
     */
    public function actionList($query)
    {
        $decoded = urldecode($query);
        $models = Dream::find()->where("title like '%$decoded%'")->all();
        $items = [];

        foreach ($models as $model) {
            $items[] = ['name' => $model->title];
        }
        Yii::$app->response->format = Response::FORMAT_JSON;

        return $items;
    }

    /**
     * Updates an existing Dream model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Сон сохранен!');
            return $this->redirect(['update', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Dream model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    
    
    public function actionImport()
    {
        $dataProvider = new ArrayDataProvider([
            'pagination' => [
                'pageSize' => 0,
            ]
        ]);
        
        $importFolderPath = null;
      
        if (Yii::$app->request->isPost && $import_src = UploadedFile::getInstanceByName('import_src')) {
            $result = DreamImportForm::getModelsFromImportZipFile($import_src);
            $dataProvider->allModels = $result["models"];
            $importFolderPath = $result["folder"];
        }
        
        return $this->render('import', [
            'dataProvider' => $dataProvider,
            'importFolderPath' => $importFolderPath
        ]);
    }
    
    public function actionImportFinal()
    {
        $models = [];
        for($idx = 0; $idx < count(Yii::$app->request->post('DreamImportForm', [])); $idx++) {
          $models[] = new DreamImportForm;
        }
        
        if(DreamImportForm::loadMultiple($models, Yii::$app->request->post())) {
            foreach($models as $model)
            {
                $model->dream_book_id = Yii::$app->request->post('dream_book_id');
                $model->save();
            }

            //Drop import folder if exis
            FileHelper::removeDirectory(Yii::$app->request->post('importFolderPath'));
            Yii::$app->session->setFlash('success', 'Импортирование выполнено успешно!');
        }
        
        return $this->redirect(['index']);
    }

    /**
     * Finds the Dream model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dream the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dream::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
