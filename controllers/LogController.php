<?php

namespace app\controllers;

use app\models\Log;
use yii\data\ActiveDataProvider;

class LogController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Log::find()->orderBy(['id' => SORT_DESC])
        ]);
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionClean()
    {
        Log::deleteAll();
        \Yii::$app->session->setFlash('success', 'Лог очищен!');
        return $this->redirect('index');
    }
}
