<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

/**
 * DreamController implements the CRUD actions for Dream model.
 */
class ImageController extends Controller
{
    public function actions()
    {
        return [
            'upload' => [
                'class' => 'trntv\filekit\actions\UploadAction',
            ],
            'delete' => [
                'class' => 'trntv\filekit\actions\DeleteAction',
            ],
            'glide' => 'trntv\glide\actions\GlideAction'
        ];
    }
}