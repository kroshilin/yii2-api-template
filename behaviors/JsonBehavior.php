<?php
namespace app\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class JsonBehavior extends Behavior
{
    public $attributes = [];
    
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'encodeJson',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'encodeJson',
            ActiveRecord::EVENT_AFTER_INSERT => 'decodeJson',
            ActiveRecord::EVENT_AFTER_UPDATE => 'decodeJson',
            ActiveRecord::EVENT_AFTER_FIND => 'decodeJson',
        ];
    }
    
    public function encodeJson()
    {
        foreach ($this->attributes as $attribute) {
            $this->owner->$attribute = !empty($this->owner->$attribute) ? Json::encode($this->owner->$attribute) : null;
        }
    }
    
    public function decodeJson()
    {
        foreach ($this->attributes as $attribute) {
            $this->owner->$attribute = !empty($this->owner->$attribute) ? Json::decode($this->owner->$attribute, true) : null;
        }
    }
}