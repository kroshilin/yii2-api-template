<?php
namespace app\behaviors;

use yii\base\Behavior;
use yii\db\ActiveRecord;

class TsToISOBehavior extends Behavior
{
    public $attributes = ['created_at', 'updated_at'];
    public $format =  "Y-m-d\TH:i:s\Z";

    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_FIND => 'encodeTs',
            ActiveRecord::EVENT_AFTER_INSERT => 'encodeTs',
            ActiveRecord::EVENT_AFTER_UPDATE => 'encodeTs',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
        ];
    }
    
    public function encodeTs()
    {
        foreach ($this->attributes as $attribute) {
            $this->owner->$attribute = !empty($this->owner->$attribute) ? date($this->format, $this->owner->$attribute) : null;
        }
    }

    public function beforeUpdate()
    {
        foreach ($this->attributes as $attribute) {
            $this->owner->$attribute = !empty($this->owner->$attribute) ?
                (!is_numeric($this->owner->$attribute) ?
                    \Yii::$app->formatter->asTimestamp($this->owner->$attribute) :
                    $this->owner->$attribute)
                : null;
        }
    }
}