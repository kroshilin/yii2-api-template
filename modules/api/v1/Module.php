<?php

namespace app\modules\api\v1;

use Yii;
use app\models\Device;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;

class Module extends \yii\base\Module
{
    public function init()
    {
        parent::init();
        Yii::$app->user->identityClass = 'app\modules\api\common\models\ApiUserIdentity';
        Yii::$app->user->enableSession = false;
        Yii::$app->user->loginUrl = null;
        // initialize the module with the configuration loaded from config.php
        Yii::configure($this, require(__DIR__ . '/config.php'));
    }
}
