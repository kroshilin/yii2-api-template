<?php

namespace app\modules\api\common\actions;

use Yii;
use yii\rest\Action;
use app\modules\api\common\models\resources\MusicTrack;
use yii\data\ActiveDataProvider;

class MusicTrackSearchAction extends Action
{
    public $prepareDataProvider;

    public function run($album_id = NULL, $title = NULL)
    {
        return $this->prepareDataProvider($album_id, $title);
    }

    protected function prepareDataProvider($album_id, $title)
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        $modelClass = $this->modelClass;

        $query = $modelClass::find();
        
        if($album_id) {
            $query->where(['album_id'=>$album_id]);
        }
        
        if($title) {
            $query->andFilterWhere(['like', 'title', $title]);
        }
        
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}