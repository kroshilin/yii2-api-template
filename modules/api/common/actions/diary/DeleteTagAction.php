<?php

namespace app\modules\api\common\actions\diary;

use Yii;

class DeleteTagAction extends \yii\rest\Action
{
    public function run($id)
    {
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        $model->load($bodyParams, '');
        $dreamsToUnlink = explode(',', $model->tagsToUnlink);
        $deleted = 0;
        foreach ($dreamsToUnlink as $dreamId) {
            if (is_int($dreamId)) {
                $deleted+=Yii::$app->db->createCommand("DELETE from dream_diary WHERE dream_id = $dreamId and diary_id = $id")->execute();
            }
        }

        return [
            'deleted' => $deleted
        ];
    }
}