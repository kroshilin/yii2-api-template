<?php

namespace app\modules\api\common\actions\diary;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * Class TagsMoodCountAction
 * @package app\modules\api\common\actions\diary
 *
 * Возвращает количество настроений из тегов, которые распознались в каждом из снов юзера
 */
class TagsMoodCountAction extends \yii\rest\Action
{
    public function run()
    {
        $query = new Query();
        $query->select('mood_id, count(*) count')
            ->from('diary d')
            ->leftJoin('dream_diary dd', 'dd.diary_id = d.id')
            ->leftJoin('dream dr', 'dr.id = dd.dream_id')
            ->where(['d.user_id' => Yii::$app->user->id])
            ->groupBy('mood_id');

        return ArrayHelper::map($query->all(), 'mood_id', 'count');
    }
}