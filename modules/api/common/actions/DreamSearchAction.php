<?php

namespace app\modules\api\common\actions;

use Yii;
use yii\rest\Action;
use app\modules\api\common\models\resources\Dream;
use yii\data\ActiveDataProvider;

class DreamSearchAction extends Action
{
    public $prepareDataProvider;

    public function run($book_id = NULL, $keyword = NULL)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider($book_id, $keyword);
    }

    protected function prepareDataProvider($book_id, $keyword)
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        $modelClass = $this->modelClass;

        $query = $modelClass::find();
        
        if($book_id) {
            $query->where(['dream_book_id'=>$book_id]);
        }
        
        if($keyword) {
            $query->andFilterWhere(['like', 'title', $keyword]);
        }
        
        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}