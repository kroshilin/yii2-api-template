<?php

namespace app\modules\api\common\actions\alarmstat;

use Yii;
use yii\data\ActiveDataProvider;


class IndexAction extends \yii\rest\IndexAction
{
    /**
     * @return ActiveDataProvider
     */
    public function run($from = null, $to = null)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        return $this->prepareDataProvider($from, $to);
    }

    /**
     * Prepares the data provider that should return the requested collection of the models.
     * @return ActiveDataProvider
     */
    protected function prepareDataProvider($from = null, $to = null)
    {
        if ($this->prepareDataProvider !== null) {
            return call_user_func($this->prepareDataProvider, $this);
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;
        $query = $modelClass::find();
        if ($from) {
            $query->where('started_at > '.(new \DateTime($from))->getTimestamp());
        }
        if ($to) {
            $query->andWhere('started_at < '.(new \DateTime($to))->getTimestamp());
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}