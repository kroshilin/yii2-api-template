<?php

namespace app\modules\api\common\actions\alarmstat;

use app\models\AlarmStat;
use app\models\Diary;
use Yii;
use yii\helpers\ArrayHelper;

class TotalAction extends \yii\rest\Action
{
    /**
     * @return ActiveDataProvider
     */
    public function run($from = null, $to = null)
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $user = Yii::$app->user->identity;
        $nights = Yii::$app->db
            ->createCommand("SELECT count(DISTINCT DATE_FORMAT(FROM_UNIXTIME(started_at), '%Y-%m-%D')) total from alarm_stat WHERE user_id = $user->id")
            ->queryOne();
        $days = Yii::$app->db
            ->createCommand("SELECT id, DATE_FORMAT(FROM_UNIXTIME(started_at), '%Y-%m-%d') date from alarm_stat WHERE user_id = $user->id")
            ->queryAll();
        $data = [
            "max_sleep_time" => $user->max_sleep_time,
            "min_sleep_time" => $user->min_sleep_time,
            "total_sleep_time" => $user->total_sleep_time,
            "nights" => $nights['total'],
            "dreams" => Yii::$app->formatter->asInteger(AlarmStat::find()->where(['user_id' => $user->id])->count()),
            "days" => $days,
        ];
        return $data;
    }
}