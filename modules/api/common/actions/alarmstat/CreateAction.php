<?php

namespace app\modules\api\common\actions\alarmstat;

use app\modules\api\common\models\resources\AlarmStat;
use Yii;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

class CreateAction extends \yii\rest\CreateAction
{
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /* @var $model AlarmStat */
        $model = new $this->modelClass([
            'scenario' => $this->scenario,
        ]);

        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        $model->load($bodyParams, '');
        $model->motions = array_key_exists('motions', $bodyParams) ? Json::encode($bodyParams['motions']) : '';
        $model->factors = array_key_exists('factors', $bodyParams) ? Json::encode($bodyParams['factors']) : '';
        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}