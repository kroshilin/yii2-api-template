<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\rest\ActiveController;

class DreamController extends ActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\Dream';
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['index'] = [
            'class' => 'app\modules\api\common\actions\DreamSearchAction',
            'modelClass' => $this->modelClass
        ];
        
        return $actions;
    }
}