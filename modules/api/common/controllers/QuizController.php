<?php

namespace app\modules\api\common\controllers;

use app\modules\api\common\models\resources\QuestionResult;
use app\modules\api\common\models\resources\Quiz;
use app\modules\api\common\models\resources\QuizResult;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\ForbiddenHttpException;

class QuizController extends BaseAuthController
{
    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query' => Quiz::find(),
        ]);
    }

    public function actionView($id)
    {
        return $this->findModel($id);
    }

    public function actionResult($id)
    {
        if (!Yii::$app->user->can('user')) {
            throw new ForbiddenHttpException;
        }
        if (Yii::$app->request->isPost) {
            return QuestionResult::saveResults(Yii::$app->request->bodyParams);
        } else {
            return QuestionResult::calculatePoints($id);
        }
    }

    /**
     * Returns the data model based on the primary key given.
     * If the data model is not found, a 404 HTTP exception will be raised.
     * @param string $id the ID of the model to be loaded. If the model has a composite primary key,
     * the ID must be a string of the primary key values separated by commas.
     * The order of the primary key values should follow that returned by the `primaryKey()` method
     * of the model.
     * @return ActiveRecordInterface the model found
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function findModel($id)
    {
        /* @var $modelClass ActiveRecordInterface */
        $model = Quiz::findOne($id);

        if (isset($model)) {
            return $model;
        } else {
            throw new NotFoundHttpException("Тест на найден: $id");
        }
    }
}