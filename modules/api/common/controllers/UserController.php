<?php

namespace app\modules\api\common\controllers;

use app\modules\api\common\models\AuthForm;
use app\modules\api\common\models\resources\User;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Json;
use yii\web\HttpException;
use yii\web\MethodNotAllowedHttpException;
use yii\web\ServerErrorHttpException;

class UserController extends BaseAuthController
{
    public $modelClass = 'app\modules\api\common\models\resources\User';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['settings', 'profile', 'avatar', 'delete-avatar'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
                [
                    'actions' => ['auth', 'get-code'],
                    'allow' => true,
                    'roles' => ['?']
                ]
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'avatar' => ['post'],
                'register' => ['post'],
                'get-code' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function actionSettings()
    {
        $request  = Yii::$app->request;
        $userObj  = Yii::$app->user->identity;

        if ($request->isGet) {
            return Json::decode($userObj->alarm_settings);
        } elseif ($request->isPost) {
            $userObj->alarm_settings = $request->rawBody;

            if($userObj->update()){
                return $userObj->alarm_settings;
            } else {
                throw new ServerErrorHttpException($userObj->getFirstError() ? : "Не удалось обновить данные" );
            }
        }

        throw new MethodNotAllowedHttpException("Поддерживаются только POST и GET запросы");
    }

    public function actionAuth()
    {
        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        $model = new AuthForm();
        $model->load($bodyParams, '');
        if (!$model->validate()) {
            return $model;
        }
        $user = $model->createApiUser();
        if (YII_DEBUG) {
            return ['profile' => $user,'code' => $model->generateCode()];
        }
        return $user;
    }

    public function actionGetCode()
    {
        $model = new AuthForm();
        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        $model->load($bodyParams, '');
        if (!$model->validate()) {
            return $model;
        }
        if (YII_DEBUG) {
            return $model->generateCode();
        }
        if ($model->sendCode()) {
            return ['Код отправлен'];
        } else {
            throw new ServerErrorHttpException('Ошибка при отправке кода');
        }
    }

    public function actionProfile()
    {
        if (Yii::$app->getRequest()->isGet) {
            return User::findOne(Yii::$app->user->identity->id);
        }
        $bodyParams = Yii::$app->getRequest()->getBodyParams();
        $model = User::findOne(Yii::$app->user->identity->id);
        $model->load($bodyParams, '');
        $model->username = Yii::$app->user->identity->username;
        $model->save();
        return $model;
    }

    public function actionAvatar($fileparam)
    {
        $file = \yii\web\UploadedFile::getInstanceByName($fileparam);
        if (!$file || !$file->size || $file->error) {
            return 'Error: Upload error';
        }
        if (!in_array($file->type, array('image/jpeg', 'image/png'))) {
            return 'Error: Invalid file type';
        }
        Yii::$app->user->identity->thumbnail_path = Yii::$app->fileStorage->save($file);
        Yii::$app->user->identity->thumbnail_base_url = Yii::$app->fileStorage->baseUrl;
        Yii::$app->user->identity->save(false);
        return User::findOne(Yii::$app->user->identity->id);
    }

    public function actionDeleteAvatar($thumbnail_path)
    {
        if (!Yii::$app->fileStorage->delete($thumbnail_path)) {
            throw new HttpException(400);
        }
        Yii::$app->user->identity->thumbnail_path = null;
        Yii::$app->user->identity->thumbnail_base_url = null;
        Yii::$app->user->identity->save(false);
        return User::findOne(Yii::$app->user->identity->id);
    }
}