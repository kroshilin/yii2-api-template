<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class DiaryController extends BaseAuthActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\Diary';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['create', 'update', 'index', 'delete', 'view', 'delete-tag', 'tags-mood-count'],
                    'allow' => true,
                    'roles' => ['user'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'update': $allow = Yii::$app->user->can('updateOwnDiary', ['model' => $model]); break;
            case 'delete': $allow = Yii::$app->user->can('updateOwnDiary', ['model' => $model]); break;
            default: $allow = true;
        }

        if (!$allow) {
            throw new ForbiddenHttpException;
        }
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['tags-mood-count'] = [
            'class' => 'app\modules\api\common\actions\diary\TagsMoodCountAction',
            'modelClass' => $this->modelClass
        ];
        $actions['delete-tag'] = [
            'class' => 'app\modules\api\common\actions\diary\DeleteTagAction',
            'modelClass' => $this->modelClass
        ];
        $actions['index'] = [
            'class' => 'yii\rest\IndexAction',
            'modelClass' => $this->modelClass,
            'prepareDataProvider' =>  function ($action) {
                $modelClass = $action->modelClass;
                return new ActiveDataProvider([
                    'query' => $modelClass::find()->where(['user_id' => Yii::$app->user->id])->orderBy(['id' => SORT_DESC]),
                ]);
            }
        ];
        return $actions;
    }
}