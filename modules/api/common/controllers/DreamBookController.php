<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\filters\AccessControl;
use app\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

class DreamBookController extends ActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\DreamBook';
}