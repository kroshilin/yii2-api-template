<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\rest\Controller;
use app\modules\api\common\models\resources\Serial;

class SerialController extends Controller
{
    public $modelClass = 'app\modules\api\common\models\resources\Serial';
    
    public function actionOverview()
    {
        $model = Serial::findOne(getenv('SERIAL_ROW_RETURN_ID'));
        
        return $model;
    }
}