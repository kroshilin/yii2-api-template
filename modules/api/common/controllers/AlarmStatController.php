<?php

namespace app\modules\api\common\controllers;

use yii\filters\AccessControl;
use yii\web\ForbiddenHttpException;

class AlarmStatController extends BaseAuthActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\AlarmStat';

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['create', 'index', 'view', 'total', 'delete'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case 'delete': $allow = ($model->user_id == \Yii::$app->user->id); break;
            default: $allow = true;
        }

        if (!$allow) {
            throw new ForbiddenHttpException;
        }
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['index'] = [
            'class' => 'app\modules\api\common\actions\alarmstat\IndexAction',
            'modelClass' => $this->modelClass
        ];
        $actions['create'] = [
            'class' => 'app\modules\api\common\actions\alarmstat\CreateAction',
            'modelClass' => $this->modelClass
        ];
        $actions['total'] = [
            'class' => 'app\modules\api\common\actions\alarmstat\TotalAction',
            'modelClass' => $this->modelClass
        ];
        return $actions;
    }

}