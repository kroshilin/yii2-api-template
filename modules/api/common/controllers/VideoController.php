<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\rest\ActiveController;

class VideoController extends ActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\Video';
}