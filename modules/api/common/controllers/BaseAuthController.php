<?php

namespace app\modules\api\common\controllers;

use yii\helpers\ArrayHelper;
use app\filters\auth\HttpBearerAuth;
use filsh\yii2\oauth2server\filters\ErrorToExceptionFilter;

class BaseAuthController extends \yii\rest\Controller
{
    /**
    * @inheritdoc
    */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'authenticator' => [
                'class' => HttpBearerAuth::className()
            ],
            'exceptionFilter' => [
                'class' => ErrorToExceptionFilter::className()
            ],
        ]);
    }
}