<?php

namespace app\modules\api\common\controllers;

use Yii;
use yii\filters\AccessControl;
use app\filters\auth\HttpBearerAuth;
use yii\rest\ActiveController;
use yii\web\ForbiddenHttpException;

class MusicTrackController extends ActiveController
{
    public $modelClass = 'app\modules\api\common\models\resources\MusicTrack';
    
    public function actions()
    {
        $actions = parent::actions();
        
        $actions['index'] = [
            'class' => 'app\modules\api\common\actions\MusicTrackSearchAction',
            'modelClass' => $this->modelClass
        ];
        
        return $actions;
    }    
}