<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class QuestionResult extends \app\models\QuestionResult
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }
}