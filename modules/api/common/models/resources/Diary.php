<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class Diary extends \app\models\Diary
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TsToISOBehavior::className(),
            'attributes' => ['created_at', 'updated_at', 'dreamed_at']
        ];
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['user_id']);
        return $fields;
    }

    public function extraFields()
    {
        $fields['tags'] = function($model) {
            $tags = Yii::$app->db->createCommand("SELECT dream.id id, dream.title title, origin, dream.thumbnail_path thumbnail_path, dream.description description, mood.title mood, mood.id mood_id, db.id dream_book_id, db.title dream_book_title
                                                   FROM dream
                                                   JOIN dream_diary dd ON dd.dream_id = dream.id
                                                   JOIN dream_book db ON db.id = dream.dream_book_id
                                                   LEFT JOIN mood ON mood.id = dream.mood_id
                                                   WHERE dd.diary_id = $model->id")
            ->queryAll();
            $resultTags = [];
            foreach ($tags as $tag) {
                $tag['cover'] = Yii::$app->glide->createSignedUrl([
                    'image/glide',
                    'path' => $tag['thumbnail_path'],
                    'h' => 300
                ], true);
                unset($tag['thumbnail_path']);
                $resultTags[] = $tag;
            }
            return $resultTags;
        };
        $fields['relatedVideo'] = function($model) {
            $tagsIntersect = Yii::$app->params['videoTagsIntersection']/100;
            $related = Video::find()->where("id IN (SELECT dv.video_id
                                                        FROM dream_diary dd
                                                        INNER JOIN
                                                        dream_video dv on dv.dream_id = dd.dream_id
                                                        WHERE dd.diary_id = $model->id
                                                        GROUP BY dv.video_id
                                                        HAVING COUNT(dd.dream_id) >= (SELECT COUNT(dd.dream_id) FROM dream_diary dd WHERE dd.diary_id = $model->id) * $tagsIntersect)")->all();
            return $related;
        };
        return $fields;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDreams()
    {
        return $this->hasMany(Dream::className(), ['id' => 'dream_id'])
            ->viaTable('dream_diary', ['diary_id' => 'id']);
    }

}