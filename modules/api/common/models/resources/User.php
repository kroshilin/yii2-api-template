<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class User extends \app\models\User
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['password_hash']);
        unset($fields['auth_key']);
        unset($fields['password_reset_token']);
        unset($fields['status']);
        unset($fields['alarm_settings']);
        unset($fields['thumbnail_base_url']);
        $fields['cover'] = function ($model) {
            return $model->thumbnail_path ? Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $model->thumbnail_path,
                'h' => 300
            ], true) : null;
        };
        return $fields;
    }
}