<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class Serial extends \app\models\Serial
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['id']);
        unset($fields['thumbnail_path']);
        unset($fields['thumbnail_base_url']);
        unset($fields['created_at']);
        unset($fields['updated_at']);
        $fields['cover'] = function ($model) {
            return Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $this->thumbnail_path,
                'h' => 300
            ], true);
        };
        return $fields;
    }
}