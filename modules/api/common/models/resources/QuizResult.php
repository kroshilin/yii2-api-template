<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class QuizResult extends \app\models\QuizResult
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['quiz_id']);
        return $fields;
    }
}