<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class Quiz extends \app\models\Quiz
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TsToISOBehavior::className()
        ];
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['thumbnail_path']);
        unset($fields['thumbnail_base_url']);
        $fields['results'] = function ($model) {
            return $model->results;
        };
        $fields['cover'] = function ($model) {
            return Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $this->thumbnail_path,
                'h' => 300
            ], true);
        };
        return $fields;
    }

    public function extraFields()
    {
        return ['questions'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['quiz_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResults()
    {
        return $this->hasMany(QuizResult::className(), ['quiz_id' => 'id']);
    }

}