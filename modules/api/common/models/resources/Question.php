<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class Question extends \app\models\Question
{
    public function fields()
    {
        $fields = parent::fields();
        $fields['options'] = function ($model) {
            return $model->answers;
        };
        return $fields;
    }
}