<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class MusicTrack extends \app\models\MusicTrack
{
    // public function behaviors()
    // {
        // $behaviors = parent::behaviors();
        // $behaviors[] = TsToISOBehavior::className();
        // return $behaviors;
    // }

    public function fields()
    {
        $fields = parent::fields();
        
        unset($fields['id']);
        
        unset($fields['music_track_path']);
        unset($fields['music_track_base_url']);
        $fields['media_src'] = function ($model) {
            return $this->music_track_base_url . '/' . $this->music_track_path;
        };
        
        unset($fields['thumbnail_path']);
        unset($fields['thumbnail_base_url']);
        $fields['cover'] = function ($model) {
            return Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $this->thumbnail_path,
                'h' => 300
            ], true);
        };
        
        return $fields;
    }

}