<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;
use yii\helpers\Json;

class AlarmStat extends \app\models\AlarmStat
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = [
            'class' => TsToISOBehavior::className(),
            'attributes' => ['created_at', 'updated_at', 'started_at', 'ended_at']
        ];
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        $fields['motions'] = function($model) {
            return Json::decode($model->motions);
        };
        $fields['factors'] = function($model) {
            return Json::decode($model->factors);
        };

        return $fields;
    }
}