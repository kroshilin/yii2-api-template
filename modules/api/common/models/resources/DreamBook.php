<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class DreamBook extends \app\models\DreamBook
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['thumbnail_path']);
        unset($fields['thumbnail_base_url']);
        $fields['cover'] = function ($model) {
            return Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $this->thumbnail_path,
                'h' => 300
            ], true);
        };
        return $fields;
    }

    public function extraFields()
    {
        return ['dreams'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDreams()
    {
        return $this->hasMany(Dream::className(), ['dream_book_id' => 'id']);
    }
}