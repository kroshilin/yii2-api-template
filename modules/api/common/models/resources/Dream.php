<?php

namespace app\modules\api\common\models\resources;

use app\behaviors\TsToISOBehavior;
use Yii;

class Dream extends \app\models\Dream
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors[] = TsToISOBehavior::className();
        return $behaviors;
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['thumbnail_path']);
        unset($fields['thumbnail_base_url']);
        $fields['cover'] = function ($model) {
            return Yii::$app->glide->createSignedUrl([
                'image/glide',
                'path' => $model->thumbnail_path,
                'h' => 300
            ], true);
        };
        $fields['mood_id'];
        return $fields;
    }
}