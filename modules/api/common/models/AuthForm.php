<?php
/**
 * Created by PhpStorm.
 * User: krosh
 * Date: 20.02.2016
 * Time: 15:27
 */
namespace app\modules\api\common\models;

use app\modules\api\common\models\resources\User;
use Yii;


class AuthForm extends \yii\base\Model
{
    public $username;

    public function rules()
    {
        return [
            [['username'], 'required'],
            ['username', 'integer'],
        ];
    }

    public function createApiUser()
    {
        $user = User::findByUsername($this->username);
        if (!$user) {
            $user = new User([
                'username' => $this->username
            ]);
            $user->generateAuthKey();
            $user->setPassword(Yii::$app->security->generateRandomString());
            $user->save();
        }
        return $user;
    }

    public function sendCode()
    {
        return Yii::$app->sms->send($this->generateCode());
    }

    public function generateCode()
    {
        $code = Yii::$app->security->generateRandomString((integer)Yii::$app->params['smsCodeLength']);
        Yii::$app->cache->set((integer)$this->username, $code, Yii::$app->params['smsCodeExpiration']);
        return $code;
    }
}