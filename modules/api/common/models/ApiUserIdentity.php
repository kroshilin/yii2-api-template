<?php

namespace app\modules\api\common\models;

use Yii;
use app\models\User;
use yii\filters\RateLimitInterface;

/**
 * Created by PhpStorm.
 * User: Anton
 * Date: 26.11.2015
 * Time: 9:46
 */
class ApiUserIdentity extends User implements RateLimitInterface
{

    /**
     * Returns the maximum number of allowed requests and the window size.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @return array an array of two elements. The first element is the maximum number of allowed requests,
     * and the second element is the size of the window in seconds.
     */
    public function getRateLimit($request, $action)
    {
        return [
            Yii::$app->params['api']['allowedRequests'],
            Yii::$app->params['api']['perTime']
        ];
    }

    /**
     * Loads the number of allowed requests and the corresponding timestamp from a persistent storage.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @return array an array of two elements. The first element is the number of allowed requests,
     * and the second element is the corresponding UNIX timestamp.
     */
    public function loadAllowance($request, $action)
    {
        return [
            Yii::$app->cache->get($this->getCacheKey('api_rate_allowance')),
            Yii::$app->cache->get($this->getCacheKey('api_rate_timestamp'))
        ];
    }

    /**
     * Saves the number of allowed requests and the corresponding timestamp to a persistent storage.
     * @param \yii\web\Request $request the current request
     * @param \yii\base\Action $action the action to be executed
     * @param integer $allowance the number of allowed requests remaining.
     * @param integer $timestamp the current timestamp.
     */
    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        Yii::$app->cache->set($this->getCacheKey('api_rate_allowance'), $allowance, Yii::$app->params['api']['perTime']);
        Yii::$app->cache->set($this->getCacheKey('api_rate_timestamp'), $timestamp, Yii::$app->params['api']['perTime']);
    }

    /**
     * @param $key
     * @return array
     */
    public function getCacheKey($key)
    {
        return [__CLASS__, $this->getId(), $key];
    }
}
