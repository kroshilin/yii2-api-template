<?php

// All Deployer recipes are based on `recipe/common.php`.
require 'recipe/common.php';

localServer('local-server-prod')
    ->env('branch', 'master')
    ->env('deploy_path', '/var/www/dreams/prod');

localServer('local-server-test')
    ->env('composer_options', 'install --verbose --prefer-dist --optimize-autoloader --no-progress --no-interaction')
    ->env('branch', 'dev')
    ->env('deploy_path', '/var/www/dreams/test');

set('repository', 'git@gitlab.globus-ltd.com:a.kroshilin/dreams.git');
set('shared_dirs', [
    'runtime',
    'web/uploads',
    'web/assets'
]);

set('shared_files', [
    '.env',
    'tests/codeception/.env'
]);

/**
 * Run migrations
 */
task('deploy:run_migrations', function () {
    run('php {{release_path}}/yii migrate up --interactive=0');
})->desc('Run migrations');

task('deploy:restart_php', function () {
    run('sudo service php5-fpm restart');
});

task('test', function () {
    run('php {{release_path}}/tests/codeception/bin/yii migrate up --interactive=0');
    run('cd {{release_path}}/tests; codecept build; codecept run unit; codecept run functional;');
})->desc('Run tests for application.')
    ->onlyOn('local-server-test');

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'deploy:vendors',
    'test',
    'deploy:run_migrations',
    'deploy:symlink',
    'deploy:restart_php',
    'cleanup',
])->desc('Deploy Dreams!');




after('deploy', 'success');
