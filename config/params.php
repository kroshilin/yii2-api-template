<?php
    return [
        'adminEmail' => getenv('ADMIN_EMAIL'),
        'robotEmail' => getenv('ROBOT_EMAIL'),
        'hostname' => getenv('FRONTEND_URL'),
        "yii.migrations"=> [
            "@yii/rbac/migrations",
            "@vendor/filsh/yii2-oauth2-server/migrations",
            "@yii/log/migrations"
        ],
        "rutubeVideoUrl" => getenv('RUTUBE_VIDEO_URL'),
        "wordMinLength" => getenv('WORD_MIN_LENGTH'),
        "levenshteinRatio" => getenv('LEVENSHTEIN_RATIO'),
        "videoTagsIntersection" => getenv('VIDEO_TAGS_INTERSECTION'),
        'api' => [
            'allowedRequests' => getenv('ALLOWED_REQUESTS'),
            'perTime' => getenv('PER_TIME'),
        ],
        "smsCodeLength" => getenv('SMS_CODE_LENGTH'),
        "smsCodeExpiration" => getenv('SMS_CODE_EXPIRATION')
    ];
