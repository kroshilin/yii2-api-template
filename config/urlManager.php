<?php
    return [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
            'v1/overview' => 'v1/serial/overview',
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/dream'], 'only' => ['index', 'view']],
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/dream-book'], 'only' => ['index', 'view']],
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/video'], 'only' => ['index', 'view']],
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/diary']],
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/alarm-stat'], 'only' => ['index', 'view', 'create', 'delete'], 'pluralize' => false],
            ['class' => 'yii\rest\UrlRule', 'controller' => ['v1/quiz'], 'only' => ['index', 'view']],
            '<module:\w+-?\w+>/quizzes/result/<id:\d+>' => '<module>/quiz/result',
            '<module:\w+-?\w+>/diaries/mood' => '<module>/diary/tags-mood-count',
            '<module:\w+-?\w+>/diaries/delete-tag/<id:\d+>' => '<module>/diary/delete-tag',
            'POST oauth2/<action:\w+>' => 'oauth2/rest/<action>',
            '<controller:\w+-?\w+>s' => '<controller>/index',
            '<controller:\w+-?\w+>/<id:\d+>' => '<controller>/view',
            '<controller:\w+-?\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
            '<module:\w+-?\w+>/<controller:\w+-?\w+>/<action:\w+>/<id:\d+>' => '<module>/<controller>/<action>',
            '<module:\w+-?\w+>/<controller:\w+-?\w+>s/<action:\w+>' => '<module>/<controller>/<action>',
        ],
    ];