<?php
Yii::setAlias('@frontendUrl', getenv('FRONTEND_URL'));

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$log = require(__DIR__ . '/log.php');
$sphinx = require(__DIR__ . '/sphinx.php');
$urlManager = require(__DIR__ . '/urlManager.php');

$config = [
    'id' => 'dreams',
    'language' => 'ru-RU',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'site/index',
    'modules' => [
        'oauth2' => [
            'class' => 'filsh\yii2\oauth2server\Module',
            'tokenParamName' => 'accessToken',
            'tokenAccessLifetime' => 3600 * 24 * 30 * 12 * 10,
            'controllerMap' => [
                'rest' => 'app\modules\oauth2\controllers\RestController'
            ],
            'storageMap' => [
                'user_credentials' => 'app\models\User',
            ],
            'grantTypes' => [
                'user_credentials' => [
                    'class' => 'OAuth2\GrantType\UserCredentials',
                ],
                'refresh_token' => [
                    'class' => 'OAuth2\GrantType\RefreshToken',
                    'always_issue_new_refresh_token' => true
                ]
            ]
        ],
        'v1' => [
            'class' => 'app\modules\api\v1\Module',
        ],
        'admin' => [
            'class' => 'mdm\admin\Module',
            'as access' => [
                'class' => '\yii\filters\AccessControl',
                'rules' => [
                    [
                        'controllers' => ['admin/role','admin/permission','admin/rule','admin/assignment'],
                        'allow' => true,
                        'roles' => ['admin']
                    ]

                ]
            ]
        ]
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'defaultRoles' => ['user'],
        ],
        'consoleRunner' => [
            'class' => 'vova07\console\ConsoleRunner',
            'file' => '@app/yii' // or an absolute path to console file
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => getenv('COOKIE_VALIDATION_KEY'),
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ]
        ],
        'response' => [
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'encodeOptions' => JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'glide' => [
            'class' => 'trntv\glide\components\Glide',
            'sourcePath' => '@app/web/uploads',
            'cachePath' => '@runtime/glide',
            'signKey' => getenv('GLIDE_SIGN_KEY')
        ],
        'fileStorage'=>[
            'class' => 'trntv\filekit\Storage',
            'baseUrl' => '@frontendUrl/uploads',
            'filesystem'=> [
                'class' => 'app\components\LocalFlysystemBuilder',
                'path' => '@webroot/uploads'
            ]
        ],
        'sms' => [
            'class' => 'app\components\SmsOnline',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'sphinx' => $sphinx,
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
              'class' => 'yii\swiftmailer\Mailer',
              // send all mails to a file by default. You have to set
              // 'useFileTransport' to false and configure a transport
              // for the mailer to send real emails.
              'useFileTransport' => true,
        ],
        'log' => $log,
        'security' => [
            'passwordHashStrategy' => 'password_hash'
        ],
        'urlManager' => $urlManager,
        'db' => $db,
    ],
    'params' => $params,
];

if (!YII_ENV_PROD) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => ['192.168.*']
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['192.168.*']
    ];
}

return $config;
