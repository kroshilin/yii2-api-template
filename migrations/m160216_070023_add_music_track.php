<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_070023_add_music_track extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('music_track', [
            'id' => $this->primaryKey(),
            'album_id' => $this->integer(),
            'number' => $this->integer(),
            'title' => $this->string(255),
            'duration' => $this->integer(),
            'description' => $this->text(),
            'music_track_path' => $this->string(255),
            'music_track_base_url' => $this->string(255),
            'thumbnail_path' => $this->string(255),
            'thumbnail_base_url' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
        
        $this->addForeignKey('fk_track_album', 'music_track', 'album_id', 'music_album', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160216_070023_add_music_track cannot be reverted.\n";

        return false;
    }
}
