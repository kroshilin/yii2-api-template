<?php

use yii\db\Schema;
use yii\db\Migration;

class m160217_075323_add_frequency_to_dreams extends Migration
{
    public function up()
    {
        $this->addColumn('dream', 'frequency', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m160217_075323_add_frequency_to_dreams cannot be reverted.\n";

        return false;
    }
}
