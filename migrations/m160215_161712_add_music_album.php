<?php

use yii\db\Schema;
use yii\db\Migration;

class m160215_161712_add_music_album extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('music_album', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);
    }

    public function down()
    {
        echo "m160215_161712_add_music_album cannot be reverted.\n";

        return false;
    }
}
