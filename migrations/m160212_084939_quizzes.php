<?php

use yii\db\Schema;
use yii\db\Migration;

class m160212_084939_quizzes extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('quiz', [
            'id' => $this->primaryKey(),
            'status' => $this->boolean(),
            'title' => $this->string(),
            'description' => $this->text(),
            'thumbnail_path' => $this->string(255),
            'thumbnail_base_url' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ],$tableOptions);

        $this->createTable('quiz_result', [
            'id' => $this->primaryKey(),
            'quiz_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'min_points' => $this->integer(),
            'max_points' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ],$tableOptions);

        $this->createTable('quiz_question',[
            'id' => $this->primaryKey(),
            'quiz_id' => $this->integer(),
            'title' => $this->string(),
            'description' => $this->text(),
            'options' => $this->text(),
        ], $tableOptions);

        $this->createTable('quiz_question_result', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'question_id' => $this->integer(),
            'points' => $this->integer(),
            'answer' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        // FK for quiz and quiz_result
        $this->addForeignKey('fk_quiz_result_quiz_id', 'quiz_result', 'quiz_id', 'quiz', 'id', 'cascade', 'cascade');

        // FK for quiz and question connection
        $this->addForeignKey('fk_quiz_question_quiz_id', 'quiz_question', 'quiz_id', 'quiz', 'id', 'cascade', 'cascade');

        // FK for results, user and quiz connection
        $this->addForeignKey('fk_quiz_question_result_question_id', 'quiz_question_result', 'question_id', 'quiz_question', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fk_quiz_question_result_user_id', 'quiz_question_result', 'user_id', 'user', 'id', 'cascade', 'cascade');

    }

    public function down()
    {
        $this->dropTable('quiz_result');
        $this->dropTable('quiz_question_result');
        $this->dropTable('quiz_question');
        $this->dropTable('quiz');
    }
}
