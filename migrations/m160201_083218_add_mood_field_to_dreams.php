<?php

use yii\db\Schema;
use yii\db\Migration;

class m160201_083218_add_mood_field_to_dreams extends Migration
{
    public function up()
    {
        $this->addColumn('dream', 'mood_id', $this->integer());
        $this->addForeignKey('fk_mood_dream', 'dream', 'mood_id', 'mood', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160201_083218_add_mood_field_to_dreams cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
