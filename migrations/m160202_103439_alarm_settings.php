<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_103439_alarm_settings extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'alarm_settings', Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m160202_103439_alarm_settings cannot be reverted.\n";

        return false;
    }
}
