<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_133039_add_dream_video extends Migration
{
    public function up()
    {
        $this->createTable('dream_video', [
            'dream_id' => $this->integer(),
            'video_id' => $this->integer()
        ]);

        $this->addForeignKey('dream_video_dreamfk', 'dream_video', 'dream_id', 'dream', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('dream_video_videofk', 'dream_video', 'video_id', 'video', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160210_133039_add_dream_video cannot be reverted.\n";

        return false;
    }
}
