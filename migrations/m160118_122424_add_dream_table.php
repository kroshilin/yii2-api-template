<?php

use yii\db\Schema;
use yii\db\Migration;

class m160118_122424_add_dream_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('dream', [
            'id' => $this->primaryKey(),
            'dream_book_id' => $this->integer(),
            'title' => $this->string(255),
            'description' => $this->text(),
            'text' => $this->text(),
            'thumbnail_path' => $this->string(255),
            'thumbnail_base_url' => $this->string(255),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ], $tableOptions);

        $this->addForeignKey('fk_dream_dream_book', 'dream', 'dream_book_id', 'dream_book', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160118_122424_add_dream_table cannot be reverted.\n";

        return false;
    }
}
