<?php

use yii\db\Schema;
use yii\db\Migration;

class m160216_184940_add_tag_origin extends Migration
{
    public function up()
    {
        $this->addColumn('dream_diary', 'origin', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m160216_184940_add_tag_origin cannot be reverted.\n";

        return false;
    }

}
