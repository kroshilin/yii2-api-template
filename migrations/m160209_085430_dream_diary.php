<?php

use yii\db\Schema;
use yii\db\Migration;

class m160209_085430_dream_diary extends Migration
{
    public function up()
    {
        $this->createTable('dream_diary', [
            'dream_id' => $this->integer(),
            'diary_id' => $this->integer()
        ]);

        $this->addForeignKey('dream_diary_dreamfk', 'dream_diary', 'dream_id', 'dream', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('dream_diary_diaryfk', 'dream_diary', 'diary_id', 'diary', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160209_085430_dream_diary cannot be reverted.\n";

        return false;
    }

}
