<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_120609_add_place_and_condition extends Migration
{
    public function up()
    {
        $this->addColumn('diary', 'place', Schema::TYPE_STRING);
        $this->addColumn('diary', 'condition', Schema::TYPE_STRING);
    }

    public function down()
    {
        echo "m160210_120609_add_place_and_condition cannot be reverted.\n";
        return false;
    }

}
