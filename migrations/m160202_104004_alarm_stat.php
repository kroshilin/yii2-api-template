<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_104004_alarm_stat extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'max_sleep_time', Schema::TYPE_INTEGER);
        $this->addColumn('user', 'min_sleep_time', Schema::TYPE_INTEGER);
        $this->addColumn('user', 'total_sleep_time', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m160202_104004_alarm_stat cannot be reverted.\n";

        return false;
    }
}
