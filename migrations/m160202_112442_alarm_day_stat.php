<?php

use yii\db\Schema;
use yii\db\Migration;

class m160202_112442_alarm_day_stat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('alarm_stat', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            "max_sleep_time" => $this->integer()->notNull(),
            "min_sleep_time" => $this->integer()->notNull(),
            "number_of_hours" => $this->integer()->notNull(),
            'dreams' => $this->text(),
            'dreamed_at' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $tableOptions);

        $this->addForeignKey('fk_user_alarm_stat', 'alarm_stat', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160202_112442_alarm_stat cannot be reverted.\n";

        return false;
    }
}
