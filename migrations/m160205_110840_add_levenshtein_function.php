<?php

use yii\db\Schema;
use yii\db\Migration;

class m160205_110840_add_levenshtein_function extends Migration
{
    public function up()
    {
        $sql = file_get_contents(__DIR__.'/../data/levenshtein.sql');
        $this->execute($sql);
    }

    public function down()
    {
        $this->execute('DROP FUNCTION levenshtein$$');
        $this->execute('DROP FUNCTION levenshtein_ratio$$');
        return true;
    }

}
