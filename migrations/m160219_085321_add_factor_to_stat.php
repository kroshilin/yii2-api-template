<?php

use yii\db\Migration;

class m160219_085321_add_factor_to_stat extends Migration
{
    public function up()
    {
        $this->addColumn('alarm_stat', 'factors', \yii\db\Schema::TYPE_TEXT);
    }

    public function down()
    {
        echo "m160219_085321_add_factor_to_stat cannot be reverted.\n";

        return false;
    }
}
