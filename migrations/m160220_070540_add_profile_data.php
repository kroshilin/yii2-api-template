<?php

use yii\db\Migration;

class m160220_070540_add_profile_data extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'thumbnail_path', \yii\db\Schema::TYPE_STRING);
        $this->addColumn('user', 'thumbnail_base_url', \yii\db\Schema::TYPE_STRING);
        $this->addColumn('user', 'name', \yii\db\Schema::TYPE_STRING);
        $this->addColumn('user', 'age', \yii\db\Schema::TYPE_INTEGER);
        $this->addColumn('user', 'gender', \yii\db\Schema::TYPE_STRING);

        $this->dropColumn('user', 'email');
    }

    public function down()
    {
        echo "m160220_070540_add_profile_data cannot be reverted.\n";

        return false;
    }
}
