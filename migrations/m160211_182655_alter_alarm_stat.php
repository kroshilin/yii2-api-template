<?php

use yii\db\Schema;
use yii\db\Migration;

class m160211_182655_alter_alarm_stat extends Migration
{
    public function up()
    {
        $this->dropColumn('alarm_stat', 'max_sleep_time');
        $this->dropColumn('alarm_stat', 'min_sleep_time');
        $this->dropColumn('alarm_stat', 'number_of_hours');
        $this->dropColumn('alarm_stat', 'dreams');
        $this->dropColumn('alarm_stat', 'dreamed_at');

        $this->addColumn('alarm_stat', 'efficiency', Schema::TYPE_FLOAT);
        $this->addColumn('alarm_stat', 'motions', Schema::TYPE_TEXT);
        $this->addColumn('alarm_stat', 'started_at', Schema::TYPE_INTEGER);
        $this->addColumn('alarm_stat', 'ended_at', Schema::TYPE_INTEGER);
        $this->addColumn('alarm_stat', 'time', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m160211_182655_alter_alarm_stat cannot be reverted.\n";

        return false;
    }
}
