<?php

use yii\db\Schema;
use yii\db\Migration;

class m160210_141610_add_episode_field_to_video extends Migration
{
    public function up()
    {
      $this->addColumn('video', 'episode', $this->integer());
    }

    public function down()
    {
        echo "m160210_141610_add_episode_field_to_video cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
