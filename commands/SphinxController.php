<?php
namespace app\commands;

use app\models\Log;
use Yii;
use yii\console\Controller;

class SphinxController extends Controller
{
    public function actionIndexer()
    {
        exec('sudo indexer --rotate --all');
        Yii::warning('Запущена индексация Sphinx');
    }
}
