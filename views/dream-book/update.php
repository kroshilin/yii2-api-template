<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DreamBook */

$this->title = 'Редактировать сонник: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сонники', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="dream-book-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
