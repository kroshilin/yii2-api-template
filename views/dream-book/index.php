<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DreamBookSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сонники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dream-book-index">

    <p>
        <?= Html::a('Добавить сонник', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Обложка',
                'value' => function ($model) {
                    return Html::img(Yii::$app->glide->createSignedUrl([
                        'image/glide',
                        'path' => $model->thumbnail_path,
                        'h' => 100
                    ], true),
                        ['class' => 'img-responsive']);
                },
                'format' => 'html'
            ],
            'title',
            'description:html',
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
