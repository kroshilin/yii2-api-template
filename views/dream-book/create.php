<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\DreamBook */

$this->title = 'Добавить сонник';
$this->params['breadcrumbs'][] = ['label' => 'Сонники', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dream-book-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
