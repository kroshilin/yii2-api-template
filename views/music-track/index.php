<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MusicTrackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Музыкальные композиции';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-track-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'album_id',
                'value' => function ($model) {
                    return (empty($model->musicAlbum) ? null : $model->musicAlbum->title );
                },
                'format' => 'html',
                'filter' => \app\models\MusicAlbum::getMusicAlbums()
            ],
            'number',
            'title',
            'duration',
            'description:html',
            [
                'label' => 'Обложка',
                'value' => function ($model) {
                    return Html::img(Yii::$app->glide->createSignedUrl([
                        'image/glide',
                        'path' => $model->thumbnail_path,
                        'h' => 100
                    ], true),
                        ['class' => 'img-responsive']);
                },
                'format' => 'html'
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
