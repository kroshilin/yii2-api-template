<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\MusicTrack */

$this->title = 'Создать';
$this->params['breadcrumbs'][] = ['label' => 'Музыкальные композиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-track-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
