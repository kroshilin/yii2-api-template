<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MusicTrack */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="music-track-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'album_id')->dropDownList(\app\models\MusicAlbum::getMusicAlbums()) ?>
    
    <?= $form->field($model, 'number')->textInput() ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'coverFile')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['image/upload'],
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 1
        ]
    );
    ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'trackFile')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['image/upload'],
            'maxFileSize' => 50 * 1024 * 1024, // 50 MiB
            'maxNumberOfFiles' => 1
        ]
    );
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
