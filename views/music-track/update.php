<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MusicTrack */

$this->title = 'Редактировать композицию: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Музыкальные композиции', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="music-track-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
