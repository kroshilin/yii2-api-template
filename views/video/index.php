<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\VideoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Видео';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="video-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить видео', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    return Html::img(Yii::$app->glide->createSignedUrl([
                        'image/glide',
                        'path' => $model->thumbnail_path,
                        'h' => 100
                    ], true),
                        ['class' => 'img-responsive'])
                    .$model->title;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncateWords($model->description, 40, '...', true);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'text',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncateWords($model->text, 40, '...', true);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'episode',
                'value' => function ($model) {
                    return $model->episode;
                },
                'format' => 'html'
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
