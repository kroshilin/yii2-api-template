<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Video */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="video-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'video_id', [
    'template' => '{label} <div class="input-group"><div class="input-group-addon">'.Yii::$app->params['rutubeVideoUrl'].'</div>{input}{error}{hint}</div>'
    ])  ?>

    <?= $model->video_id?Html::tag('iframe','', ['src' => $model->videoUrl, 'height' => '300px', 'width' => '500px']):'' ?>

    <?= $form->field($model, 'file')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['image/upload'],
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 1
        ]
    );
    ?>
    
    <?= $form->field($model, 'episode')->textInput() ?>

    <?= $form->field($model, 'tagNames')->widget(\dosamigos\selectize\SelectizeTextInput::className(), [
        // calls an action that returns a JSON object with matched
        // tags
        'loadUrl' => ['dream/list'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => false,
        ],
    ])->hint('Используйте запятые для разделения') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Добавить' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
