<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Quiz $model
 */

$questionsProvider = new \yii\data\ActiveDataProvider(['query' => $model->getQuestions()->orderBy(['id' => SORT_DESC])]);
$resultsProvider = new \yii\data\ActiveDataProvider(['query' => $model->getResults()->orderBy(['id' => SORT_DESC])]);

$this->title = 'Редактировать '. $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Тесты', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Обновить');
?>
<div class="quiz-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

    <p>
        <h3>Вопросы теста</h3>
        <?= Html::a('Добавить вопрос в этот тест', ['/question/create', 'quiz_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="questions">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $questionsProvider,
            'columns' => [
                'title',
                'description',
                [
                    'value' => function ($model) {
                        $html = Html::a(Html::tag('span','',['class' => 'glyphicon glyphicon-pencil']), ['/question/update', 'id' => $model->id]).
                            Html::a(Html::tag('span','',['class' => 'glyphicon glyphicon-trash']), ['/question/delete', 'id' => $model->id], ['data-method' => 'post']);
                        return $html;
                    },
                    'format' => 'raw'
                ]
            ]
        ]); ?>
    </div>

    <p>
    <h3>Возможные результаты</h3>
        <?= Html::a('Добавить результат прохождения теста', ['/quiz-result/create', 'quiz_id' => $model->id], ['class' => 'btn btn-success']) ?>
    </p>
    <div class="results">
        <?= \yii\grid\GridView::widget([
            'dataProvider' => $resultsProvider,
            'columns' => [
                'title',
                'description',
                'min_points',
                'max_points',
                [
                    'value' => function ($model) {
                        $html = Html::a(Html::tag('span','',['class' => 'glyphicon glyphicon-pencil']), ['/quiz-result/update', 'id' => $model->id]).
                            Html::a(Html::tag('span','',['class' => 'glyphicon glyphicon-trash']), ['/quiz-result/delete', 'id' => $model->id], ['data-method' => 'post']);
                        return $html;
                    },
                    'format' => 'raw'
                ]
            ]
        ]); ?>
    </div>

</div>
