<?php

use yii\helpers\Html;
use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var app\models\QuizSearch $searchModel
 */

$this->title = 'Тесты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-index">

    <p>
        <?= Html::a('Добавить тест', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            [
                'attribute' => 'status',
                'filter' => \app\models\Quiz::getStatuses(),
                'value' => function($model) {
                    $statuses = \app\models\Quiz::getStatuses();
                    return $statuses[$model->status];
                }
            ],
            'description:html',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{update} {delete}'],
        ],
    ]); ?>

</div>
