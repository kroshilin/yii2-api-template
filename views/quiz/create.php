<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\Quiz $model
 */

$this->title = Yii::t('app', 'Create');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Quizzes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
