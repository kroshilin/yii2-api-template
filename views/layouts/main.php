<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
                NavBar::begin([
                    'brandLabel' => 'ТВ3, Сны',
                    'brandUrl' => Yii::$app->homeUrl,
                    'options' => [
                        'class' => 'navbar-inverse navbar-fixed-top',
                    ],
                ]);

                echo Nav::widget([
                    'options' => ['class' => 'navbar-nav navbar-right'],
                    'items' => [
                        [
                            'label' => 'Сериал',
                            'url' => ['#'],
                            'visible' => Yii::$app->user->can('admin'),
                            'items' => [
                                [
                                    'label' => 'Редактировать информацию о сериале',
                                    'url' => ['/serial'],
                                ],
                            ],
                        ],
                        [
                            'label' => 'Медиа',
                            'url' => ['#'],
                            'visible' => Yii::$app->user->can('admin'),
                            'items' => [
                                [
                                    'label' => 'Видео',
                                    'url' => ['/video/index'],
                                    'visible' => Yii::$app->user->can('admin')
                                ],
                                [
                                    'label' => 'Муз. композиции', 
                                    'url' => ['/music-track/index'], 
                                    'visible' => Yii::$app->user->can('admin')
                                ],
                            ],
                        ],

                        [
                            'label' => 'Тесты',
                            'url' => ['/quiz/index'],
                            'visible' => Yii::$app->user->can('admin'),
                        ],
                        ['label' => 'Дневник снов', 'url' => ['/diary/index'], 'visible' => Yii::$app->user->can('admin')],
                        [
                            'label' => 'Сонник',
                            'url' => ['#'],
                            'visible' => Yii::$app->user->can('admin'),
                            'items' => [
                                [
                                    'label' => 'Сонники',
                                    'url' => ['/dream-book/index'],
                                ],
                                [
                                    'label' => 'Сны (ключевые слова)',
                                    'url' => ['/dream/index'],
                                ],
                                [
                                    'label' => 'Настроения',
                                    'url' => ['/moods'],
                                ]
                            ],
                        ],
                        [
                            'label' => 'Пользователи',
                            'url' => ['/site/user'],
                            'visible' => Yii::$app->user->can('admin'),
                            'items' => [
                                [
                                    'label' => 'Список пользователей',
                                    'url' => ['/site/user'],
                                ],
                                [
                                    'label' => 'Роли',
                                    'url' => ['/admin/role'],
                                ],
                                [
                                    'label' => 'Разрешения',
                                    'url' => ['/admin/permission'],
                                ],
                                [
                                    'label' => 'Правила',
                                    'url' => ['/admin/rule'],
                                ],
                                [
                                    'label' => 'Назначение ролей',
                                    'url' => ['/admin/assignment'],
                                ]
                            ]
                        ],
                        ['label' => 'Лог', 'url' => ['/log/index'], 'visible' => Yii::$app->user->can('admin')],
                        ['label' => 'Войти', 'url' => ['/site/login'], 'visible' => Yii::$app->user->isGuest],
                        [
                            'label' => 'Выйти (' . (Yii::$app->user->isGuest?'':Yii::$app->user->identity->username) . ')',
                            'url' => ['/site/logout'],
                            'linkOptions' => ['data-method' => 'post'],
                            'visible' => !Yii::$app->user->isGuest
                        ],
                    ]
                ]);
                
                NavBar::end();
            ?>

            <div class="container">
                <?= Breadcrumbs::widget([
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                ]) ?>
                <?= \app\widgets\Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <p class="pull-left">&copy; ТВ3 СНЫ <?= date('Y') ?></p>
            </div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
