<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Diary */

$this->title = 'Обновить сон: ' . ' ' . $model->title . 'пользователя' . $model->user->username;
$this->params['breadcrumbs'][] = ['label' => 'Дневник снов', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="diary-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
