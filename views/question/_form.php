<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use app\models\Question;

/**
 * @var yii\web\View $this
 * @var app\models\Question $model
 * @var yii\widgets\ActiveForm $form
 */
?>

<div class="quiz-question-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => 255]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?php $m = $model->errors ?>
    <?php if (!$model->answers) { ?>
        <?= $form->beginField($model,'answers[]',['options' => ['class' => 'copy-option well']])?>
        <h4>Вариант ответа</h4>
        <?= Html::textarea($model->formName().'[answers][0][text]', '', [
            'minHeight' => 150,
            'maxHeight' => 150,
            'class' => 'form-control',
            'placeholder' => 'Текст варианта',
            'data-index' => 0
        ])
        ?>
        <?= Html::textInput($model->formName().'[answers][0][point]', '', [
            'class' => 'form-control',
            'placeholder' => 'Количество баллов за ответ',
            'data-index' => 0
        ])
        ?>
        <?= Html::hiddenInput($model->formName().'[answers][0][id]', '0', [
            'data-index' => 0
        ])
        ?>
        <?= Html::a(Yii::t('app', 'Удалить вариант'),'#',['class' => 'btn btn-danger delete']); ?>

        <?= $form->endField($model,'answers[]')?>
     <?php } else {
        foreach ($model->answers as $key => $answer) {
        ?>
            <?= $form->beginField($model,'answers[]',['options' => ['class' => 'copy-option well']])?>
                <h4>Вариант ответа</h4>
            <?= Html::textarea($model->formName()."[answers][$key][text]", $answer['text'], [
                'minHeight' => 150,
                'maxHeight' => 150,
                'class' => 'form-control',
                'placeholder' => 'Текст варианта',
                'data-index' => $key
            ])
            ?>
            <?= Html::textInput($model->formName()."[answers][$key][point]", $answer['point'], [
                'class' => 'form-control',
                'placeholder' => 'Количество баллов за ответ',
                'data-index' => $key
            ])
            ?>
            <?= Html::hiddenInput($model->formName()."[answers][$key][id]", $answer['id'], [
                'data-index' => $key
            ])
            ?>
            <?= Html::a(Yii::t('app', 'Удалить вариант'),'#',['class' => 'btn btn-danger delete']); ?>
            <?= $form->endField($model,'answers[]')?>
    <?php }

    } ?>

    <div class="form-group">
        <a href="#" class="btn btn-primary add-option">Добавить вариант</a>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Обновить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
