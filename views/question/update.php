<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\QuizQuestion $model
 */

$this->title = 'Обновить ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="quiz-question-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
