<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var app\models\QuizQuestion $model
 */

$this->title = 'Добавить вопрос';
$this->params['breadcrumbs'][] = ['label' => 'Вопросы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="quiz-question-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
