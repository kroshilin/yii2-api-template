<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Serial */

$this->title = 'Редактировать информацию: ' . ' ' . $model->title;
/*$this->params['breadcrumbs'][] = ['label' => 'Serials', 'url' => ['index']];*/
/*$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];*/
$this->params['breadcrumbs'][] = 'Редактировать информацию о сериале';
?>
<div class="serial-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
