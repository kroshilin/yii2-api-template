<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MusicAlbumSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Музыкальные альбомы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="music-album-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'title',
            'created_at:datetime',
            'updated_at:datetime',
        ],
    ]); ?>

</div>
