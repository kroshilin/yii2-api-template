<?php
use yii\helpers\Html;
use yii\grid\GridView;
/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 */
$this->title = 'Лог событий';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="action-log-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <a class="btn btn-danger" href="<?= \yii\helpers\Url::to(['clean']);?>">Очистить лог</a>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'level',
            'log_time:datetime',
            'message'
        ],
    ]); ?>

</div>