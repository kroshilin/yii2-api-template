<?php

use yii\helpers\Html;
/*use yii\widgets\ActiveForm;*/

?>

<div class="dream-form">

    <?= Html::beginForm('', 'post', ['enctype' => 'multipart/form-data']) ?>

    <p>Выберите .zip файл, содержащий данные для импорта</p>
    
    <div class="form-group">
      <?= Html::fileInput('import_src', null, ['accept' => ".zip"]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Продолжить', ['class' => 'btn btn-primary']) ?>
    </div>

    <?= Html::endForm(); ?>

</div>
