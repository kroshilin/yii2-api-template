<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DreamSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сны';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dream-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Добавить сон', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Загрузить из файла', ['import'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'dream_book_id',
                'value' => function ($model) {
                    return Html::img(Yii::$app->glide->createSignedUrl([
                                    'image/glide',
                                    'path' => $model->dreamBook->thumbnail_path,
                                    'h' => 100
                                ], true),
                                ['class' => 'img-responsive'])
                            .$model->dreamBook->title;
                },
                'format' => 'html',
                'filter' => \app\models\DreamBook::getDreamBooks()
            ],
            [
                'attribute' => 'title',
                'value' => function ($model) {
                    return Html::img(Yii::$app->glide->createSignedUrl([
                        'image/glide',
                        'path' => $model->thumbnail_path,
                        'h' => 100
                    ], true),
                        ['class' => 'img-responsive'])
                    .$model->title;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'description',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncateWords($model->description, 40, '...', true);
                },
                'format' => 'html'
            ],
            [
                'attribute' => 'text',
                'value' => function ($model) {
                    return yii\helpers\StringHelper::truncateWords($model->text, 40, '...', true);
                },
                'format' => 'html'
            ],
            'created_at:datetime',
            'updated_at:datetime',
            [
                'attribute' => 'mood_id',
                'value' => function ($model) {
                    return (empty($model->mood) ? null : $model->mood->title );
                },
                'format' => 'html',
                'filter' => \app\models\Mood::getMoods()
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update} {delete}'
            ],
        ],
    ]); ?>

</div>
