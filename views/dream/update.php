<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Dream */

$this->title = 'Редактировать сон: ' . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Сны', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['update', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="dream-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
