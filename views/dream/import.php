<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\DreamBook;
use app\models\Mood;


$this->title = 'Импортировать сны';
$this->params['breadcrumbs'][] = ['label' => 'Сны', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Импорт';
?>
<div class="DreamImportForm-import">

    <h1>Импортировать сны</h1>

    <?php if(empty($dataProvider->allModels)) : ?>
    
        <?= $this->render('_import_form') ?>
        
    <?php else : ?>
    
    <?php $form = ActiveForm::begin(['action' => '/dream/import-final']); ?>
    
    <p>Выберите, в какой сонник импортировать записи:</p>
    <?= Html::dropDownList('dream_book_id', false, DreamBook::getDreamBooks(), ['class' => 'form-control']) ?><br>
    <p>Перед выполнением процедуры импорта, вы можете внести исправления или снять галочки у строк, которые необходимо исключить</p>
    
    <?=
      GridView::widget([
          'dataProvider' => $dataProvider,
          'summary' => '<p>Количество найденных значений: <b>{totalCount}</b></p>',
          'columns' => [
              [
                  'label' => '',
                  'format' => 'raw',
                  'value' => function($model, $idx){
                      return Html::checkbox('DreamImportForm['.$idx.'][check]', true);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'title',
                  'value' => function($model, $idx){
                      return Html::input('text', 'DreamImportForm['.$idx.'][title]', $model->title, ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'description',
                  'value' => function($model, $idx){
                      return Html::textarea('DreamImportForm['.$idx.'][description]', $model->description, ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'text',
                  'value' => function($model, $idx){
                      return Html::textarea('DreamImportForm['.$idx.'][text]', $model->text, ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'isExistInProject',
                  'value' => function($model, $idx){
                      return Html::dropDownList('DreamImportForm['.$idx.'][isExistInProject]', (int)$model->isExistInProject, [0 => 'Нет', 1 => 'Да'], ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'episodeId',
                  'value' => function($model, $idx){
                      return Html::input('number', 'DreamImportForm['.$idx.'][episodeId]', $model->episodeId, ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'episodeTitle',
                  'value' => function($model, $idx){
                      return Html::input('text', 'DreamImportForm['.$idx.'][episodeTitle]', $model->episodeTitle, ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'mood_id',
                  'value' => function($model, $idx){
                      return Html::dropDownList('DreamImportForm['.$idx.'][mood_id]', (int)$model->mood_id, Mood::getMoods(), ['class' => 'form-control']);
                  },
              ],
              [
                  'format' => 'raw',
                  'attribute' => 'thumbnail_path',
                  'value' => function($model, $idx){
                      return Html::img($model->thumbnail_path, ['width' => '100']).Html::input('hidden', 'DreamImportForm['.$idx.'][thumbnail_path]', $model->thumbnail_base_url);
                  },
              ],
              
          ]
      ]);
    ?>
    
    <?= Html::hiddenInput('importFolderPath', $importFolderPath) ?>
    
    <div class="form-group">
        <?= Html::submitButton('Импортировать', ['class' => 'btn btn-primary']) ?>
    </div>
    
    <?php $form = ActiveForm::end(); ?>
    
    <?php endif; ?>

</div>
