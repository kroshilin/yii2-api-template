<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Dream */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dream-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dream_book_id')->dropDownList(\app\models\DreamBook::getDreamBooks()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'file')->widget(
        '\trntv\filekit\widget\Upload',
        [
            'url' => ['image/upload'],
            'maxFileSize' => 10 * 1024 * 1024, // 10 MiB
            'maxNumberOfFiles' => 1
        ]
    );
    ?>

    <?= $form->field($model, 'mood_id')->dropDownList(\app\models\Mood::getMoods()) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
