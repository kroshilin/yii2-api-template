<?php
namespace tests\codeception\_pages;
use yii\codeception\BasePage;

class SerialEditPage extends BasePage
{
    public $route = 'serial';

    public function serialEdit($title, $text, $file = null)
    {
        $this->actor->fillField('.serial-form input[name="Serial[title]"]', $title);
        $this->actor->fillField('.serial-form textarea[name="Serial[text]"]', $text);
        if($file)
        {
          $this->actor->attachFile('.serial-form input[type="file"]', $file);
        }
        $this->actor->click('button[type="submit"]');
    }
}