<?php
namespace tests\codeception\_pages\Mood;
use yii\codeception\BasePage;

class MoodListingPage extends BasePage
{
    public $route = 'mood';

    public function addNewMood($title)
    {
        $this->actor->click('.add-mood-button');
        $this->actor->fillField('input[name="Mood[title]"]', $title);
        $this->actor->click('BUTTON[type="submit"]');
    }

    public function updateTestMood($title, $newTitle)
    {
        $this->actor->click('TABLE TR:last-child TD:last-child A[title="Редактировать"]');
        $this->actor->see('Редактировать запись настроения: '.$title);
        $this->actor->SeeInField('input[name="Mood[title]"]', $title);
        $this->actor->fillField('input[name="Mood[title]"]', $newTitle);
        $this->actor->click('BUTTON[type="submit"]');
    }

    public function deleteTestMood($title)
    {
        $this->actor->see($title, 'TABLE TR:last-child');
        $deleteLink = $this->actor->grabAttributeFrom('TABLE TR:last-child TD:last-child A[title="Удалить"]', 'href');
        $this->actor->sendAjaxPostRequest($deleteLink);
    }
}