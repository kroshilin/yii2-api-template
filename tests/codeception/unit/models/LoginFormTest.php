<?php

namespace tests\codeception\unit\models;

use Yii;
use yii\codeception\TestCase;
use app\models\LoginForm;
use tests\codeception\fixtures\UserFixture;

class LoginFormTest extends TestCase
{
    use \Codeception\Specify;

    protected function tearDown()
    {
        Yii::$app->user->logout();
        parent::tearDown();
    }

    public function testLoginNoUser()
    {
        $model = new LoginForm([
            'username' => 'not_existing_username',
            'password' => 'not_existing_password',
        ]);

        $this->specify('user should not be able to login, when there is no identity', function () use ($model) {
            $this->assertFalse($model->login(), 'model should not login user');
            $this->assertTrue(Yii::$app->user->isGuest, 'user should not be logged in');
        });
    }

    public function testLoginWrongPassword()
    {
        $model = new LoginForm([
            'username' => 'demo',
            'password' => 'wrong_password',
        ]);

        $this->specify('user should not be able to login with wrong password', function () use ($model) {
            $this->assertFalse($model->login(),'model should not login user');
            $this->assertNotEmpty($model->errors['password'],'error message should be set');
            $this->assertTrue(Yii::$app->user->isGuest, 'user should not be logged in');
        });
    }

    public function testLoginCorrect()
    {
        $model = new LoginForm([
            'username' => 'bayer.hudson',
            'password' => 'password_0',
        ]);

        $this->specify('user should be able to login with correct credentials', function () use ($model) {
            $this->assertTrue($model->login(), 'model should login user');
            $this->assertFalse(isset($model->errors['password']), 'error message should not be set');
            $this->assertFalse(Yii::$app->user->isGuest, 'user should be logged in');
        });
    }

    /**
     * @inheritdoc
     */
    public function fixtures()
    {
        return [
            'user' => [
                'class' => UserFixture::className(),
                'dataFile' => '@tests/codeception/fixtures/data/user.php'
            ],
        ];
    }


}
