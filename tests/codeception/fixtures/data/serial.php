<?php
return [
    [
        'title' => 'fixture_title',
        'text' => 'fixture_text',
        'thumbnail_path' => 'fixture_thumbnail_path',
        'thumbnail_base_url' => 'fixture_thumbnail_base_url',
        'created_at' => 'fixture_created_at',
        'updated_at' => 'fixture_updated_at',
    ],
];