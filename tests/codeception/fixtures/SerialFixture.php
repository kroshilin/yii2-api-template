<?php
namespace tests\codeception\fixtures;
use yii\test\ActiveFixture;

class SerialFixture extends ActiveFixture
{
    public $modelClass = 'app\models\Serial';
}