<?php
namespace tests\codeception\frontend\functional;
use tests\codeception\_pages\LoginPage;
class LoginCest
{
    /**
     * This method is called before each test method.
     *
     * @param \Codeception\Event\TestEvent $event
     */
    public function _before($event)
    {
    }
    /**
     * This method is called after each test method, even if test failed.
     *
     * @param \Codeception\Event\TestEvent $event
     */
    public function _after($event)
    {
    }
    /**
     * This method is called when test fails.
     *
     * @param \Codeception\Event\FailEvent $event
     */
    public function _fail($event)
    {
    }

    /**
     * Test if active user can login with username/password combo.
     *
     * @param $I
     */
    public function testLoginWithUsername($I)
    {
        $I->wantTo('ensure that active user can login with username');
        $loginPage = LoginPage::openBy($I);
        //-- submit form with no data --//
        $I->amGoingTo('(login with username): submit login form with no data');
        $loginPage->login('', '');
        $I->expectTo('see validations errors');
        $I->see('Необходимо заполнить «Username».', '.help-block');
        $I->see('Необходимо заполнить «Password».', '.help-block');
        //-- submit form with wrong credentials --//
        $I->amGoingTo('(login with username): try to login with wrong credentials');
        $loginPage->login('wrong', 'wrong');
        $I->expectTo('see validations errors');
        $I->see('Incorrect username or password.', '.help-block');
        //-- login user with correct credentials --//
        $I->amGoingTo('try to log in correct user');
        $loginPage->login('bayer.hudson', 'password_0');
        $I->expectTo('see that user is logged in');
        $I->seeLink('Выйти (bayer.hudson)');
        $I->dontSeeLink('Login');
        $I->dontSeeLink('Signup');
    }
}