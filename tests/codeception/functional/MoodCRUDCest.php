<?php
namespace tests\codeception\frontend\functional;
use tests\codeception\_pages\Mood\MoodListingPage as MoodListingPage;

class MoodCRUDCest
{
    /** Вижу страницу со списком **/
    public function showListingPage($I)
    {
      $I->wantTo('make sure that the listing page is opened.');

      $Page = $this->openListingPage($I);
    }
    
    /** Добавилась новая запись **/
    public function addingNewMoodIsSuccessful($I)
    {
      $I->wantTo('make sure that the adding a new mood is successful.');

      $Page = $this->openListingPage($I);
      $Page->addNewMood('TestMoodTitle');
      
      $I->see('Запись добавлена!', '.alert-success');
      $I->SeeInField('INPUT[name="Mood[title]"]', 'TestMoodTitle');
    }
    
    /** Вижу добавленную запись в списке **/
    public function readingTestMoodInListing($I)
    {
      $I->wantTo('make sure that i see added test mood in listing.');

      $Page = $this->openListingPage($I);
      $I->see('TestMoodTitle', 'TABLE TR:last-child');
    }
    
    /** Узменилась добавленная запись **/
    public function updatingTestMoodIsSuccessful($I)
    {
      $I->wantTo('make sure that the updating a test mood is successful.');

      $Page = $this->openListingPage($I);
      $Page->updateTestMood('TestMoodTitle', 'UpdatedTestMoodTitle');
      
      $I->see('Запись сохранена!', '.alert-success');
      $I->SeeInField('INPUT[name="Mood[title]"]', 'UpdatedTestMoodTitle');
    }
    
    /** Удалилась измененная запись **/
    public function deletingTestMoodIsSuccessful($I)
    {
      $I->wantTo('make sure that the deleting a test mood is successful.');

      $Page = $this->openListingPage($I);
      $Page->deleteTestMood('UpdatedTestMoodTitle');
      
      $I->amOnPage('moods');
      $I->dontSee('UpdatedTestMoodTitle', 'TABLE TR:last-child');
    }
    
    /** FUNCTIONS **/
    private function openListingPage($I)
    {
      return MoodListingPage::openBy($I);
    }
    
}
