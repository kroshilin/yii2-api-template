<?php
namespace tests\codeception\frontend\functional;
use tests\codeception\_pages\SerialEditPage;

class SerialEditCest
{
    public function changesSaveTest($I)
    {
      $I->wantTo('ensure that i can save changes');
      $SerialEditPage = SerialEditPage::openBy($I);
      //-- submit form with no data --//
      $I->amGoingTo('(save changes): submit form with no data');
      $SerialEditPage->serialEdit('', '');
      $I->expectTo('see validations errors');
      $I->see('Необходимо заполнить «Заголовок».', '.serial-form .help-block');
      $I->see('Необходимо заполнить «Описание».', '.serial-form .help-block');
      //-- submit form with data --//
      $I->amGoingTo('(save changes): submit form with correct data');
      $SerialEditPage->serialEdit('Название серии', 'Описание серии', 'test-pic.jpg'); // file is stored in 'tests/_data/'
      $I->expectTo('see that data was changed successfully');
      $I->see('Информация сохранена!', '.alert-success');
      $I->SeeInField('.serial-form input[name="Serial[title]"]', 'Название серии');
      $I->SeeInField('.serial-form textarea[name="Serial[text]"]', 'Описание серии');
    }
}